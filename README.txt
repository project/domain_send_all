
-- SUMMARY --

The Domain Send All is a modules developed to address specific conditions while
using DA and active/inactive sites for staging. 

Use case:
An admin wants to change affiliate publishing option of �send to all� for nodes
from multiple sites based on content type. This could be run after the sites
have been live for a while, and many nodes have been added, or when a site is
just turned live and you want to update certain content type nodes to have
send to all checked.

For a full description of the module, visit the project page:
  http://drupal.org/project/domain_send_all

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/domain_send_all


-- REQUIREMENTS --

None.
 
 
-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* User must have 'administer domains' permission.


-- TROUBLESHOOTING --

* If the menu item does not display, try clearing cache and refreshing the page.


-- CONTACT --

Current maintainers:
* Aaron Deutsch (aisforaaron) - http://drupal.org/user/116113