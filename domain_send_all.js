// Domain Send All Javascript Functions

// onclick event to check nodes on domain_send_all form
function domain_send_all_select_defaults(){
  // clear boxes first
  domain_send_all_remove_defaults(false);
  // select
  var defaults_array = Drupal.settings.domain_send_all.default_content_types;
  for (i=0; i<defaults_array.length; i++) {
    $('#edit-content-types-' + defaults_array[i]).attr('checked',true);
  }
}

// onclick event to check nodes on domain_send_all form
function domain_send_all_select_reverse_defaults(){
  // clear boxes first
  domain_send_all_remove_defaults(false);
  // select
  var reverse_defaults = Drupal.settings.domain_send_all.reverse_defaults;
  for (i=0; i<reverse_defaults.length; i++) {
    $('#edit-content-types-' + reverse_defaults[i]).attr('checked',true);
  }
}

// clear content types checkboxes
function domain_send_all_remove_defaults(op){
 $('input[id~="edit-content-types"]').attr('checked',op)
}